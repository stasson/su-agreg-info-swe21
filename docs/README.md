# Radio Hybride - Analyse Logicielle

## Overview

Le but de ce projet est de répondre a un [appel d'offre pour une application de
radio hybride]. Ce document presente l'analyse des cas d'utilisation de
l'application, le design architectural et la conception détaillée répondant au
cahier des charge suivants:

![exemple d'interface utilisateur]

1. La radio devra avoir une interface minimaliste. Un swipe vers la droite ou la
   gauche permet de sélectionner la station suivante ou précédente
   respectivement

1. Un appui sur le bouton central permet de couper ou de rétablir le son de la
   station sélectionnée

1. le nom de la station sélectionnée ainsi que le logo seront être affichés

1. La source, c'est a dire le mode de diffusion DAB, FM ou WEB) en cours sera
   affiché

1. Si le titre et l'artiste en cours de diffusions seront affichés s'ils sont
   connus

1. Le tuner choisi sera par default le tuner numérique (DAB) si le signal est
   present, le tuner FM si le signal est assez for ( > 60 dB). Si aucune de ces
   condition est réunie et si la bande passante resaux est suffisante,
   l'application aura recours a un streaming en ligne.

<!-- links -->

[appel d'offre pour une application de radio hybride]:
  ./assets/aggreg-info-ext-2021-case-study.pdf
[exemple d'interface utilisateur]: ./assets/user-interface.png

## Analyse des cas d'utilisations

```plantuml

actor "Utilisateur" as User
actor "Tuner DAB" as DAB
actor "Tuner FM" as FM
actor "Web API" as WEB

rectangle RadioApp {
  usecase "Écouter une Radio" as UC_RADIO
  usecase "Afficher les infos de la Station" as UC_INFO
  usecase "Sélectionner une Station" as UC_SELECT
  usecase "Écouter une Radio Numérique" as UC_DAB
  usecase "Écouter une Radio FM" as UC_FM
  usecase "Écouter une Radio Web" as UC_WEB
  usecase "Écouter une Radio avec la meilleur Qualite d'Ecoute" as UC_BEST
}

User -> UC_RADIO
UC_DAB <- DAB
UC_FM <- FM
UC_WEB <- WEB

UC_DAB --|> UC_RADIO  : extends
UC_FM  ---|> UC_RADIO  : extends
UC_WEB ----|> UC_RADIO  : extends


UC_BEST <|-l- UC_RADIO   : includes
UC_RADIO --|> UC_SELECT  : includes
UC_RADIO --|> UC_INFO : includes

 UC_DAB <-- UC_BEST     : includes
 UC_FM <--- UC_BEST     : includes
 UC_WEB <---- UC_BEST     : includes


note as N_BEST
  if (DAB is available) then source uses DAB
  else if (FM is available & FM level >= 60 dB) use FM
  else if ( DownloadSpeed >= 128 kbps) then use WebAPI
  else mute
end note

UC_BEST - N_BEST
```

## Analyse architecturale

> La section architecture permet de presenter les grandes lignes de
> l'architecture, en particulier la _vue physique_ et les _vues de
> développements_. ![les 4 vues architecturale](./assets/4_1_vue.png). la vues
> logique et la vue processus seront presentees dans les sections suivantes, use
> case par use case.

### Vue physique

L' application Radio est déployée sur le système multimedia. Elle permet de
controller le Tuner numérique (DAB), le Tuner analogique (FM) ou de se connecter
au _back-end web_ via les drivers fournis et la stack HTTP.

```plantuml

node RadioPlayerSystem as SoC {

  component HybridRadioApp #lightblue
  component FmDriver
  component DabDriver
  component HttpStack
}

node TunerFM

node TunerDAB


cloud WebRadioBackend {
  component WebRadioAPI
}

FmDriver -- TunerFM
DabDriver -- TunerDAB
HttpStack -- WebRadioAPI

HybridRadioApp --> FmDriver : uses
HybridRadioApp --> DabDriver : uses
HybridRadioApp --> HttpStack : uses
```

### Vue de development

#### Contraintes de conception

> Nous présumons ici que les drivers sont fournis avec l'es interfaces de
  programmations suivantes.

##### DAB Tuner driver API

```plantuml
hide empty members

class DabTunerDriver {
  getStationList(): List<Station>
  getStationById(id:String)
  subscribe(onStationChange: Function<Station>)
  play(id:String)
  pause()
}

class DabTunerDriver.Station {
  id: String
  name: String
  getLogo(): Image
}

DabTunerDriver .> DabTunerDriver.Station 
```

##### FM Tuner driver API

```plantuml
hide empty members

class FmTunerDriver {
  getStationList(): List<DabTunerStation>
  getStationById(id:String) DabTunerStation
  subscribe(onStationChange: Function<DabTunerStation>)
  play(id:String)
  pause()
}

class FmTunerDriver.Station {
  id: String
  name: String
  frequency: String
  level: Int
}

FmTunerDriver .> FmTunerDriver.Station 

```

##### Web Driver API

```plantuml
hide empty members

class  WebApi {
  getStationList(): List<WebStation>
  getStationById(id:String): WebStation
  getIcon(iconUrl: String): Image
  play(streamUrl: String)
  pause()
}

class WebStation {
  id: String
  name: String
  iconUrl: String 
  streamUrl: String
}

WebApi .> WebStation
```

##### UI Framework

```plantuml
hide empty members

class UiElement {
  id: String
  text: String
  icon: Image
  onTap(callback: Function<UiElement>)
  onSwipeLeft(callback: Function<UiElement>)
  onSwipeRight(callback: Function<UiElement>)
  findElementById(id: String) : UiElement
}

class Ui {
  <<static>> findElementById(id: String) : UiElement
}

Ui -> UiElement 

```

#### Diagramme de packetage

> L'organisation du code en couche selon les preceptes de la
> `Clean Architecture` et l'application des patrons de conception du
> `Domain Driven Design` permet de s'assurer d'un fort niveau de maintenabilité
> par une stricte separation des problèmes et l'inversion de contrôle.

```plantuml
hide empty members

package radio_hybride {

  folder ui {
    class UiElement #lightgrey
    class RadioView
    class RadioViewModel
    RadioView *--> RadioViewModel
    RadioView <.. RadioViewModel : notifies
    RadioView -> UiElement : layout
  }

  note as MVVM
    Presentation layer
    according to Model View Viewmodel
  end note

  MVVM - ui

  folder app {
    class RadioStation
    class RadioPlayer 
    class RadioService
    class RadioRepository

    RadioPlayer -> RadioRepository
    RadioPlayer <-- RadioService
    RadioRepository <-- RadioService
    RadioRepository *->  RadioStation   
  }

  folder infra {
    class DabTunerAdapter
    class FmTunerAdapter
    class WebTunerAdapter

    class DabTunerDriver #lightgrey
    class FmTunerDriver #lightgrey
    class HttpDriver #lightgrey
  }

  RadioService <--- DabTunerAdapter
  RadioService <--- FmTunerAdapter 
  RadioService <--- WebTunerAdapter

  note as DDD
    application layer
    as per Domain Driven Design
  end note

  app - DDD
  DDD --- infra

}

DabTunerAdapter *--> DabTunerDriver
FmTunerAdapter *--> FmTunerDriver
WebTunerAdapter *--> HttpDriver

RadioViewModel --> RadioPlayer  
RadioViewModel <.. RadioPlayer : notifies

```

### Conception Détaillée

#### Afficher les infos de la Station

```gherkin
Feature: Afficher les infos de la Station

  En tant qu'utilisateur,
  je veux pouvoir voir le nom de la station et le logo
  afin de savoir ce que je vais écouter

  Scenario: Afficher le nom et l'icône de la station

    ÉTANT DONNE QUE l'application est lancée
                 ET qu'aucune station n'est encore sélectionnée 
              ALORS rien n'est affichée

    ÉTANT DONNE QUE l'application est lancée
              QUAND une station est sélectionnée
              ALORS le nom de la station et le logos sont affichés

```

##### Vue process

```plantuml
actor User
participant View
participant ViewModel
participant RadioPlayer

ViewModel -> ViewModel: init()
...
ViewModel -> RadioPlayer: getStation()
ViewModel --> View: render()
```

##### Test d'integration

```python
# test/spec/page_object.py
import Ui

def get_station_name(): 
  Ui.find_element_by_id("station_name").text

def get_station_logo(): 
  Ui.find_element_by_id("station_logo").icon
```

```python
# test/spec/radio_fixture.py
class TestStation:
  def __init__(self, id, name):
    self.id = id
    self.name = name

def get_radio_fixture():
  return [
    RadioStationFixture("radio:france-inter", "France Inter"),
    RadioStationFixture("radio:fip", "FIP"),
    RadioStationFixture("radio:france-musique", "France Musique"),
  ]
```

```python
# test/spec/test_display_station.py
import pytest
import page_object
import get_radio_list from radio_fixture
import DabTunerDriver from drivers
import FmTunerDriver from drivers
import WebTunerDriver from drivers


def test_display_no_station_is_selected:
  assert not page_object.get_station_name()
  pass

def test_display_station_when_station_is_is_selected:
  # patch drivers
  DabTunerDriver.get_station_list  = get_radio_list
  FmTunerDriver.get_station_list  = get_radio_list
  WebTunerDriver.get_station_list  = get_radio_fixture

  # assert curren station is not empty
  assert page_object.get_station_name()
  pass
```

##### Test unitaires

```python
import radio/ui/viewmodel

# test/unit/ui/viewmodel.py
def test_viewmodel_initialization():
  viewmodel = ViewModel(None)
  assert viewModel.text equal ""
  assert viewModel.icon equal None

class PlayerMock:
  def __init__():
    self.text = "text"
    self.icon = "icon"

class ViewMock:
  def __init__():
    self.calbackCount = 0
  
  def render():
    self.calbackCount ++

def test_viewmodel_update():
  viewMock = PlayerMock()
  playerMock = PlayerMock()
  viewmodel = ViewModel(playerMock)
  viewmodel = subscribe(viewMock)
  viewmodel.refresh()

  assert viewMock.text == playerMock.text
  assert viewMock.logo == playerMock.logo
  assert viewMock.callbackCount == 1


# test/unit/app/player.py
# TODO: test player

# test/unit/app/repository.py
# TODO: test repository

# test/unit/app/station.py
# TODO: test station
```

##### Code

```python
# radio/ui/view.py
class View
  def __init__(self, viewmodel)
    self.viewmodel = viewmodel

  def render():
    Ui.find_element_by_id("station_name").text = viewmode.name
    Ui.find_element_by_id("station_logo").icon = viewmode.logo


# radio/ui/view_model.py
class ViewModel
  def __init__(self, player):
      self._player = player
      self._callbacks = []
      self.text = ""
      self.icon = None

  def subscribe(self, callback):
    self.callbacks.append(callback)

  def refresh():
    self.text = self._player.text
    self.icon = self._player.icon
    for callback in self._callbacks:
      calback(self)

# radio/app/player.py
# TODO: implement player

# radio/app/repository.py
# TODO: implement repository

# radio/app/station.py
# TODO: implement station
```
