# Cas d'étude de conception d'application - Radio Hybride

Ce projet presente un cas d'étude de conception d'une d'application dans le
cadre du [cours de genie logicielle] de la _préparation a l'agrégation
informatique 2021, Sorbone Université_.

Le contenu du projet est le suivant:

- [Analyse Logicielle](./docs/README.md)

## Resources

- [Cours de genie logicielle](./docs/assets/aggreg-info-ext-2021-swe-crash-course.pdf)
- [Markdown Cheatsheet](https://gitlab.com/-/snippets/2189528)
